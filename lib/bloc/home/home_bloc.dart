import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shoppy_rabbit/data/model/entities/banner.dart';
import 'package:shoppy_rabbit/data/model/entities/offer.dart';
import 'package:shoppy_rabbit/data/model/entities/product.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_failure.dart';
import 'package:shoppy_rabbit/data/repositories/home_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

enum HomeBlocStateType {
  loaded,
  loading,
  error,
}

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final UserHomeRepository _userHomeRepository;
  HomeBloc({UserHomeRepository? userHomeRepository})
      : _userHomeRepository = userHomeRepository ?? UserHomeRepositoryImp(),
        super(const HomeInitial()) {
    on<HomeStartedEvent>((event, emit) async {
      await _loadProducts(event, emit);
      await _loadOffers(event, emit);
      await _loadBanners(event, emit);
    });
  }

  Future<void> _loadProducts(
      HomeStartedEvent event, Emitter<HomeState> emit) async {
    emit(const HomeProductState.loading());
    final result = await _userHomeRepository.fetchRecommendedProducts();
    if (result.isSuccess) {
      emit(HomeProductState.loaded(result.data as List<Product>));
    } else {
      emit(HomeProductState.failure(result.error as DataLoadingFailure));
    }
  }

  Future<void> _loadOffers(
      HomeStartedEvent event, Emitter<HomeState> emit) async {
    emit(const HomeOfferState.loading());
    final result = await _userHomeRepository.fetchOffers();
    if (result.isSuccess) {
      emit(HomeOfferState.loaded(result.data as List<Offer>));
    } else {
      emit(HomeOfferState.failure(result.error as DataLoadingFailure));
    }
  }

  Future<void> _loadBanners(
      HomeStartedEvent event, Emitter<HomeState> emit) async {
    emit(const HomeBannerState.loading());
    final result = await _userHomeRepository.fetchPromotionBanners();
    if (result.isSuccess) {
      emit(HomeBannerState.loaded(result.data as List<Banner>));
    } else {
      emit(HomeBannerState.failure(result.error as DataLoadingFailure));
    }
  }
}
