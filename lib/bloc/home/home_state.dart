part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {
  const HomeInitial();
}

class HomeProductState extends HomeState {
  const HomeProductState._(this.stateType, {this.products, this.failure});
  const HomeProductState.loading() : this._(HomeBlocStateType.loading);
  const HomeProductState.loaded(List<Product> products)
      : this._(HomeBlocStateType.loaded, products: products);
  const HomeProductState.failure(DataLoadingFailure failure)
      : this._(HomeBlocStateType.error, failure: failure);
  final HomeBlocStateType stateType;
  final List<Product>? products;
  final DataLoadingFailure? failure;
  @override
  List<Object> get props => [stateType];
}

class HomeOfferState extends HomeState {
  const HomeOfferState._(this.stateType, {this.offers, this.failure});
  const HomeOfferState.loading() : this._(HomeBlocStateType.loading);
  const HomeOfferState.loaded(List<Offer> offers)
      : this._(HomeBlocStateType.loaded, offers: offers);
  const HomeOfferState.failure(DataLoadingFailure failure)
      : this._(HomeBlocStateType.error, failure: failure);
  final HomeBlocStateType stateType;
  final List<Offer>? offers;
  final DataLoadingFailure? failure;
  @override
  List<Object> get props => [stateType];
}

class HomeBannerState extends HomeState {
  const HomeBannerState._(this.stateType, {this.banners, this.failure});
  const HomeBannerState.loading() : this._(HomeBlocStateType.loading);
  const HomeBannerState.loaded(List<Banner> banners)
      : this._(HomeBlocStateType.loaded, banners: banners);
  const HomeBannerState.failure(DataLoadingFailure failure)
      : this._(HomeBlocStateType.error, failure: failure);
  final HomeBlocStateType stateType;
  final List<Banner>? banners;
  final DataLoadingFailure? failure;
  @override
  List<Object> get props => [stateType];
}
