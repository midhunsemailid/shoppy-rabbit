part of 'explore_bloc.dart';

abstract class ExploreEvent extends Equatable {
  const ExploreEvent();

  @override
  List<Object> get props => [];
}

class ExploreStartedEvent extends ExploreEvent {}

class ExploreFilterEvent extends ExploreEvent {
  const ExploreFilterEvent({
    required this.isSelected,
    required this.category,
  });
  final bool isSelected;
  final ProductCategory category;
}
