import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shoppy_rabbit/data/model/entities/product.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_failure.dart';
import 'package:shoppy_rabbit/data/repositories/explore_repository.dart';

part 'explore_event.dart';
part 'explore_state.dart';

enum ExploreBlocStateType {
  loaded,
  loading,
  error,
}

class ExploreBloc extends Bloc<ExploreEvent, ExploreState> {
  final ExploreRepository _exploreRepository;
  ExploreBloc({ExploreRepository? exploreRepository})
      : _exploreRepository = exploreRepository ?? ExploreRepositoryImp(),
        super(ExploreInitial()) {
    on<ExploreStartedEvent>((event, emit) async {
      emit(const ExploreLoadCategoriesState.loading());
      await _exploreRepository.load();
      emit(ExploreLoadCategoriesState.loaded(_exploreRepository.allCategories,
          _exploreRepository.selectedCategories));
      final result = _exploreRepository.fetchFilteredProducts();
      emit(ExploreLoadCategoriesState.loaded(_exploreRepository.allCategories,
          _exploreRepository.selectedCategories));
      if (result.isSuccess) {
        emit(ExploreLoadFilteredProductsState.loaded(
            result.data as List<Product>));
      }
    });

    on<ExploreFilterEvent>((event, emit) async {
      emit(const ExploreLoadFilteredProductsState.loading());
      if (event.isSelected) {
        _exploreRepository.selectCategory(event.category);
      } else {
        _exploreRepository.unSelectCategory(event.category);
      }
      final result = _exploreRepository.fetchFilteredProducts();
      emit(ExploreLoadCategoriesState.loaded(_exploreRepository.allCategories,
          _exploreRepository.selectedCategories));
      if (result.isSuccess) {
        emit(ExploreLoadFilteredProductsState.loaded(
            result.data as List<Product>));
      }
    });
  }
}
