part of 'explore_bloc.dart';

abstract class ExploreState extends Equatable {
  const ExploreState();

  @override
  List<Object> get props => [];
}

class ExploreInitial extends ExploreState {}

class ExploreLoadCategoriesState extends ExploreState {
  final ExploreBlocStateType stateType;
  const ExploreLoadCategoriesState._(
    this.stateType, {
    this.allCategories,
    this.selectedCategories,
    this.failure,
  });
  const ExploreLoadCategoriesState.loading()
      : this._(ExploreBlocStateType.loading);
  const ExploreLoadCategoriesState.loaded(List<ProductCategory> allCategories,
      List<ProductCategory> selectedCategories)
      : this._(ExploreBlocStateType.loaded,
            allCategories: allCategories,
            selectedCategories: selectedCategories);
  const ExploreLoadCategoriesState.failure(DataLoadingFailure failure)
      : this._(ExploreBlocStateType.error, failure: failure);

  final List<ProductCategory>? allCategories;
  final List<ProductCategory>? selectedCategories;
  final DataLoadingFailure? failure;
  @override
  List<Object> get props => [stateType];
}

class ExploreLoadFilteredProductsState extends ExploreState {
  final ExploreBlocStateType stateType;
  const ExploreLoadFilteredProductsState._(this.stateType,
      {this.products, this.failure});
  const ExploreLoadFilteredProductsState.loading()
      : this._(ExploreBlocStateType.loading);
  const ExploreLoadFilteredProductsState.loaded(List<Product> products)
      : this._(ExploreBlocStateType.loaded, products: products);
  const ExploreLoadFilteredProductsState.failure(DataLoadingFailure failure)
      : this._(ExploreBlocStateType.error, failure: failure);
  final DataLoadingFailure? failure;
  final List<Product>? products;
  @override
  List<Object> get props => [stateType];
}
