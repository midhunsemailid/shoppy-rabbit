/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 17:30:37

*/

part of '../home_page.dart';

class OfferView extends StatelessWidget {
  const OfferView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      buildWhen: (previous, current) => current is HomeOfferState,
      builder: (context, state) {
        if (state is HomeOfferState) {
          switch (state.stateType) {
            case HomeBlocStateType.loaded:
              final offers = state.offers ?? [];
              return OfferCarouselView(offers: offers);
            case HomeBlocStateType.loading:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: const CircularProgressIndicator(),
                ),
              );
            case HomeBlocStateType.error:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(state.failure?.description ??
                      'Some unexpected error happened'),
                ),
              );
          }
        }
        return const SliverPadding(padding: EdgeInsets.zero);
      },
    );
  }
}

class OfferCarouselView extends StatelessWidget {
  const OfferCarouselView({
    Key? key,
    required this.offers,
  }) : super(key: key);

  final List<Offer> offers;

  Widget _buildItem(BuildContext context, Offer offer, double width) {
    return SizedBox(
      width: width,
      child: Card(
        margin: EdgeInsets.zero,
        child: CachedNetworkImage(
          imageUrl: offer.image,
          fit: BoxFit.cover,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                      color: Colors.black, value: downloadProgress.progress)),
          errorWidget: (context, url, error) => Container(
            alignment: Alignment.center,
            child: const Icon(Icons.error),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: LayoutBuilder(builder: (context, constraints) {
        return SizedBox(
          height: 120,
          child: ListView.builder(
            padding: const EdgeInsets.only(left: 12.0, right: 12.0),
            scrollDirection: Axis.horizontal,
            itemCount: offers.length,
            itemBuilder: (context, index) =>
                _buildItem(context, offers[index], constraints.maxWidth - 30),
          ),
        );
      }),
    );
  }
}
