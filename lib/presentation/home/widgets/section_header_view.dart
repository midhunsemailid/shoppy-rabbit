/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 18:11:12

*/
part of '../home_page.dart';

class HomeSectionHeaderView extends StatelessWidget {
  const HomeSectionHeaderView({Key? key, required this.heading})
      : super(key: key);
  final String heading;
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        color: Colors.white,
        padding: const EdgeInsets.only(top: 16.0, left: 12.0, bottom: 12.0),
        child: Text(
          heading,
          style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
