/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 17:52:54

*/

part of '../home_page.dart';

class BannerView extends StatelessWidget {
  const BannerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      buildWhen: (previous, current) => current is HomeBannerState,
      builder: (context, state) {
        if (state is HomeBannerState) {
          switch (state.stateType) {
            case HomeBlocStateType.loaded:
              final banners = state.banners ?? [];
              return BannerCarouselView(banners: banners);
            case HomeBlocStateType.loading:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: const CircularProgressIndicator(),
                ),
              );
            case HomeBlocStateType.error:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(state.failure?.description ??
                      'Some unexpected error happened'),
                ),
              );
          }
        }
        return const SliverPadding(padding: EdgeInsets.zero);
      },
    );
  }
}

class BannerCarouselView extends StatelessWidget {
  BannerCarouselView({
    Key? key,
    required this.banners,
  }) : super(key: key);

  final List<ProductBanner.Banner> banners;
  final _controller = PageController(viewportFraction: 1.0, keepPage: true);

  Widget _buildItem(
      BuildContext context, ProductBanner.Banner banner, double width) {
    return SizedBox(
      width: width,
      child: Card(
        margin: EdgeInsets.zero,
        child: CachedNetworkImage(
          imageUrl: banner.image,
          fit: BoxFit.cover,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                      color: Colors.black, value: downloadProgress.progress)),
          errorWidget: (context, url, error) => Container(
            alignment: Alignment.center,
            child: const Icon(Icons.error),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final itemCount = banners.length;
    return SliverToBoxAdapter(
      child: LayoutBuilder(builder: (context, constraints) {
        return Column(
          children: [
            SizedBox(
              height: 220,
              child: PageView.builder(
                controller: _controller,
                scrollDirection: Axis.horizontal,
                itemCount: banners.length,
                itemBuilder: (context, index) => _buildItem(
                    context, banners[index], constraints.maxWidth - 30),
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            if (itemCount > 1)
              SmoothPageIndicator(
                controller: _controller,
                count: itemCount,
                effect: const WormEffect(
                  dotHeight: 8,
                  dotWidth: 8,
                  activeDotColor: Colors.black,
                  type: WormType.thin,
                  strokeWidth: 5,
                ),
              ),
            const SizedBox(
              height: 10.0,
            ),
          ],
        );
      }),
    );
  }
}
