/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 15:08:25

*/

part of '../home_page.dart';

class ProductsView extends StatelessWidget {
  const ProductsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      buildWhen: (previous, current) => current is HomeProductState,
      builder: (context, state) {
        if (state is HomeProductState) {
          switch (state.stateType) {
            case HomeBlocStateType.loaded:
              final products = state.products ?? [];
              return ProductsGridView(products: products);
            case HomeBlocStateType.loading:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: const CircularProgressIndicator(),
                ),
              );
            case HomeBlocStateType.error:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(state.failure?.description ??
                      'Some unexpected error happened'),
                ),
              );
          }
        }
        return const SliverPadding(padding: EdgeInsets.zero);
      },
    );
  }
}
