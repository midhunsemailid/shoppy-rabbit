/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 13:26:02

*/
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shoppy_rabbit/bloc/home/home_bloc.dart';
import 'package:shoppy_rabbit/data/model/entities/offer.dart';
import 'package:shoppy_rabbit/data/model/entities/banner.dart' as ProductBanner;
import 'package:shoppy_rabbit/presentation/utils/products_grid_view.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
part 'widgets/products_view.dart';
part 'widgets/offer_view.dart';
part 'widgets/banner_view.dart';
part 'widgets/section_header_view.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final HomeBloc _homeBloc;
  @override
  void initState() {
    super.initState();
    _homeBloc = HomeBloc();
    _homeBloc.add(HomeStartedEvent());
  }

  @override
  void dispose() {
    super.dispose();
    _homeBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _homeBloc,
      child: const CustomScrollView(
        slivers: [
          SliverAppBar(
              automaticallyImplyLeading: false,
              leadingWidth: 0.0,
              title: Text(
                'Home',
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w800),
              )),
          HomeSectionHeaderView(
            heading: 'Today\'s Deals',
          ),
          BannerView(),
          HomeSectionHeaderView(
            heading: 'Offers',
          ),
          OfferView(),
          HomeSectionHeaderView(
            heading: 'Recommended For You',
          ),
          ProductsView(),
        ],
      ),
    );
  }
}
