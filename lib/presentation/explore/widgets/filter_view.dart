/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 19:44:46

*/
part of '../explore_page.dart';

class FilterView extends StatelessWidget {
  const FilterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExploreBloc, ExploreState>(
      buildWhen: (previous, current) => current is ExploreLoadCategoriesState,
      builder: (context, state) {
        if (state is ExploreLoadCategoriesState) {
          switch (state.stateType) {
            case ExploreBlocStateType.loaded:
              final allCategories = state.allCategories ?? [];
              final selectedCategories = state.selectedCategories ?? [];
              final chips = allCategories.map((category) {
                final isSelected = selectedCategories.contains(category);
                return Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: FilterChip(
                    disabledColor: Colors.white,
                    backgroundColor: Colors.white,
                    selectedColor: Colors.black,
                    showCheckmark: false,
                    labelStyle: isSelected
                        ? const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)
                        : const TextStyle(color: Colors.black),
                    label: Text(category.name),
                    selected: isSelected,
                    onSelected: (bool selected) {
                      BlocProvider.of<ExploreBloc>(context).add(
                          ExploreFilterEvent(
                              isSelected: selected, category: category));
                    },
                  ),
                );
              }).toList();

              return SliverToBoxAdapter(
                child: Container(
                  padding: const EdgeInsets.all(6.0),
                  color: Colors.grey.shade200,
                  child: Column(
                    children: [
                      Container(
                        child: const Text(
                          'Categories ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.only(
                            left: 16.0, bottom: 8.0, top: 8.0),
                      ),
                      Wrap(
                        children: chips,
                      ),
                    ],
                  ),
                ),
              );
            case ExploreBlocStateType.loading:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: const CircularProgressIndicator(),
                ),
              );
            case ExploreBlocStateType.error:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(state.failure?.description ??
                      'Some unexpected error happened'),
                ),
              );
          }
        }
        return const SliverPadding(padding: EdgeInsets.zero);
      },
    );
  }
}
