/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 20:03:51

*/
part of '../explore_page.dart';

class FilteredProductsView extends StatelessWidget {
  const FilteredProductsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ExploreBloc, ExploreState>(
      buildWhen: (previous, current) =>
          current is ExploreLoadFilteredProductsState,
      builder: (context, state) {
        if (state is ExploreLoadFilteredProductsState) {
          switch (state.stateType) {
            case ExploreBlocStateType.loaded:
              final products = state.products ?? [];
              return ProductsGridView(products: products);
            case ExploreBlocStateType.loading:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: const CircularProgressIndicator(),
                ),
              );
            case ExploreBlocStateType.error:
              return SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(state.failure?.description ??
                      'Some unexpected error happened'),
                ),
              );
          }
        }
        return const SliverPadding(padding: EdgeInsets.zero);
      },
    );
  }
}
