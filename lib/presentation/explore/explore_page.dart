/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 13:25:55

*/
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shoppy_rabbit/bloc/explore/explore_bloc.dart';
import 'package:shoppy_rabbit/presentation/utils/products_grid_view.dart';
part 'widgets/filter_view.dart';
part 'widgets/filtered_product_view.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({Key? key}) : super(key: key);
  @override
  State<ExplorePage> createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  late final ExploreBloc _exploreBloc;
  @override
  void initState() {
    super.initState();
    _exploreBloc = ExploreBloc();
    _exploreBloc.add(ExploreStartedEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _exploreBloc,
      child: const CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            automaticallyImplyLeading: false,
            leadingWidth: 0.0,
            title: Text(
              'Explore',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w800),
            ),
          ),
          FilterView(),
          FilteredProductsView()
        ],
      ),
    );
  }
}
