/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 13:21:09

*/

import 'package:flutter/material.dart';
import 'package:shoppy_rabbit/presentation/cart/cart_page.dart';
import 'package:shoppy_rabbit/presentation/explore/explore_page.dart';
import 'package:shoppy_rabbit/presentation/home/home_page.dart';
import 'package:shoppy_rabbit/presentation/profile/profile_page.dart';

class AppBottomNavigationPage extends StatefulWidget {
  const AppBottomNavigationPage({Key? key}) : super(key: key);

  @override
  State<AppBottomNavigationPage> createState() =>
      _AppBottomNavigationPageState();
}

class _AppBottomNavigationPageState extends State<AppBottomNavigationPage> {
  int _selectedIndex = 0;

  static const List<Widget> _pages = <Widget>[
    HomePage(),
    ExplorePage(),
    ProfilePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _selectedIndex,
        children: _pages,
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const CartPage()));
          },
          label: const Icon(
            Icons.add_shopping_cart,
          )),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Theme.of(context).colorScheme.background,
        selectedItemColor: Colors.black,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.manage_search),
            label: 'Explore',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_outlined),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
