/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 16:35:12

*/

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shoppy_rabbit/data/model/entities/product.dart';

class ProductsGridView extends StatelessWidget {
  const ProductsGridView({
    Key? key,
    required this.products,
  }) : super(key: key);
  final List<Product> products;

  Widget _buildProductCard(BuildContext context, Product product) {
    return Card(
        margin: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                child: SizedBox.expand(
                  child: CachedNetworkImage(
                    imageUrl: product.images.first,
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter,
                    progressIndicatorBuilder:
                        (context, url, downloadProgress) => Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(
                                color: Colors.black,
                                value: downloadProgress.progress)),
                    errorWidget: (context, url, error) => Container(
                      alignment: Alignment.center,
                      child: const Icon(Icons.error),
                    ),
                  ),
                ),
              ),
            ),
            ListTile(
              contentPadding: const EdgeInsets.only(left: 8.0),
              title: Text(
                product.name,
                style: DefaultTextStyle.of(context)
                    .style
                    .copyWith(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
              subtitle: Text.rich(
                TextSpan(
                  children: <TextSpan>[
                    if (product.price.isOfferAvailable)
                      TextSpan(
                        text: '₹${product.price.amount}',
                        style: const TextStyle(
                          color: Colors.red,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                    if (product.price.isOfferAvailable)
                      TextSpan(
                        text: ' ₹${product.price.offerAmount}',
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w800,
                          fontSize: 15.0,
                        ),
                      ),
                    if (!product.price.isOfferAvailable)
                      TextSpan(
                        text: '₹${product.price.amount}',
                      ),
                  ],
                ),
              ),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.only(bottom: 64.0),
      sliver: products.isEmpty
          ? SliverToBoxAdapter(
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    SizedBox(
                      height: 120.0,
                    ),
                    Icon(
                      Icons.error,
                      size: 64.0,
                    ),
                    Text('No product found for the selected categories'),
                    SizedBox(
                      height: 120.0,
                    ),
                  ],
                ),
              ),
            )
          : SliverGrid(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 2 / 3),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return _buildProductCard(context, products[index]);
                },
                childCount: products.length,
              ),
            ),
    );
  }
}
