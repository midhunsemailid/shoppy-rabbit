/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 13:26:12

*/

import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Profile',
          style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w800),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 44.0,
          ),
          const CircleAvatar(
            radius: 64.0,
            backgroundColor: Colors.black,
            child: Text(
              'MP',
              style: TextStyle(fontSize: 44.0),
            ),
          ),
          const ListTile(
            title: Text(
              'Midhun P. Mathew',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            subtitle: Text(
              'midhunsemailid@gmail.com',
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(
            height: 44.0,
          ),
          Card(
            margin: const EdgeInsets.only(left: 16.0, right: 16.0),
            child: Column(
              children: [
                InkWell(
                  onTap: () {},
                  child: const Padding(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: ListTile(
                      leading: Icon(Icons.receipt),
                      title: Text(
                        'Order History',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      trailing: Icon(
                        Icons.arrow_right,
                        size: 30,
                      ),
                    ),
                  ),
                ),
                const Divider(
                  thickness: 1.0,
                ),
                InkWell(
                  onTap: () {},
                  child: const Padding(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: ListTile(
                      leading: Icon(Icons.location_city),
                      title: Text(
                        'Address',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      trailing: Icon(
                        Icons.arrow_right,
                        size: 30,
                      ),
                    ),
                  ),
                ),
                const Divider(
                  thickness: 1.0,
                ),
                InkWell(
                  onTap: () {},
                  child: const Padding(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: ListTile(
                      leading: Icon(Icons.favorite),
                      title: Text(
                        'Favorites',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      trailing: Icon(
                        Icons.arrow_right,
                        size: 30,
                      ),
                    ),
                  ),
                ),
                const Divider(
                  thickness: 1.0,
                ),
                InkWell(
                  onTap: () {},
                  child: const Padding(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: ListTile(
                      leading: Icon(Icons.settings),
                      title: Text(
                        'Settings',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      trailing: Icon(
                        Icons.arrow_right,
                        size: 30,
                      ),
                    ),
                  ),
                ),
                const Divider(
                  thickness: 1.0,
                ),
                InkWell(
                  onTap: () {},
                  child: const Padding(
                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: ListTile(
                      leading: Icon(Icons.info),
                      title: Text(
                        'About Us',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                      trailing: Icon(
                        Icons.arrow_right,
                        size: 30,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
