/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 14:13:35

*/

import 'package:shoppy_rabbit/data/model/data_model/category_model.dart';
import 'package:shoppy_rabbit/data/model/data_model/product_model.dart';
import 'package:shoppy_rabbit/data/model/entities/product.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_failure.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_result.dart';
import 'package:shoppy_rabbit/data/model/processing_models/entity_adapters/product_entity_adapters.dart';
import 'package:shoppy_rabbit/data/utils/local_json_client.dart';

abstract class ProductDataProvider {
  Future<ProcessResult<List<Product>>> fetchProducts();
  Future<ProcessResult<List<ProductCategory>>> fetchCategories();
}

class ProductDataProviderImp extends ProductDataProvider {
  final _jsonClient = LocalJsonClient();
  @override
  Future<ProcessResult<List<Product>>> fetchProducts() async {
    try {
      final jsonString =
          await _jsonClient.loadLocalJson(LocalJsonPaths.products);
      final products = ProductModel.productsFromJson(jsonString)
          .map((productDataModel) =>
              ProductEntityAdapter.fromDataModel(productDataModel))
          .toList();
      return ProcessResult.success(products);
    } catch (e) {
      return ProcessResult.failure(DataLoadingFailure(e.toString()));
    }
  }

  @override
  Future<ProcessResult<List<ProductCategory>>> fetchCategories() async {
    try {
      final jsonString =
          await _jsonClient.loadLocalJson(LocalJsonPaths.categories);
      final categories = CategoryModel.categoriesFromJson(jsonString)
          .map((categoryDataModel) =>
              ProductCategoryEntityAdapter.fromDataModel(categoryDataModel))
          .toList();
      return ProcessResult.success(categories);
    } catch (e) {
      return ProcessResult.failure(DataLoadingFailure(e.toString()));
    }
  }
}
