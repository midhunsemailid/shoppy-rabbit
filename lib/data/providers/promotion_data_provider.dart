/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 14:14:03

*/

import 'package:shoppy_rabbit/data/model/data_model/banner_model.dart';
import 'package:shoppy_rabbit/data/model/data_model/offer_model.dart';
import 'package:shoppy_rabbit/data/model/entities/banner.dart';
import 'package:shoppy_rabbit/data/model/entities/offer.dart';
import 'package:shoppy_rabbit/data/model/processing_models/entity_adapters/banner_entity_adapter.dart';
import 'package:shoppy_rabbit/data/model/processing_models/entity_adapters/offer_entity_adapter.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_failure.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_result.dart';
import 'package:shoppy_rabbit/data/utils/local_json_client.dart';

abstract class PromotionDataProvider {
  Future<ProcessResult<List<Banner>>> fetchBanners();
  Future<ProcessResult<List<Offer>>> fetchOffers();
}

class PromotionDataProviderImp extends PromotionDataProvider {
  final _jsonClient = LocalJsonClient();

  @override
  Future<ProcessResult<List<Banner>>> fetchBanners() async {
    try {
      final jsonString =
          await _jsonClient.loadLocalJson(LocalJsonPaths.banners);
      final banners = BannerModel.bannerModelFromJson(jsonString)
          .map((bannerModel) => BannerEntityAdapter.fromDataModel(bannerModel))
          .toList();
      return ProcessResult.success(banners);
    } catch (e) {
      return ProcessResult.failure(DataLoadingFailure(e.toString()));
    }
  }

  @override
  Future<ProcessResult<List<Offer>>> fetchOffers() async {
    try {
      final jsonString = await _jsonClient.loadLocalJson(LocalJsonPaths.offers);
      final offers = OfferModel.offerModelFromJson(jsonString)
          .map((offerModel) => OfferEntityAdapter.fromDataModel(offerModel))
          .toList();
      return ProcessResult.success(offers);
    } catch (e) {
      return ProcessResult.failure(DataLoadingFailure(e.toString()));
    }
  }
}
