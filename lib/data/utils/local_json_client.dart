/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-03-08 17:36:15

*/

import 'dart:convert';
import 'package:flutter/services.dart';

/// Load JSON file from app bundle.
class LocalJsonClient {
  LocalJsonClient();
  Future<String> loadLocalJson(String filePath) async {
    try {
      final ByteData data = await rootBundle.load(filePath);
      return utf8.decode(data.buffer.asUint8List());
    } catch (e) {
      rethrow;
    }
  }
}

class LocalJsonPaths {
  static const products = 'assets/data/json/products.json';
  static const banners = 'assets/data/json/banners.json';
  static const offers = 'assets/data/json/offers.json';
  static const categories = 'assets/data/json/categories.json';
}
