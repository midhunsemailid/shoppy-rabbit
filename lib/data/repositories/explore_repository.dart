/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 18:46:29

*/

import 'package:shoppy_rabbit/data/model/entities/product.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_result.dart';
import 'package:shoppy_rabbit/data/providers/product_data_provider.dart';

abstract class ExploreRepository {
  ProcessResult<List<Product>> fetchFilteredProducts();
  Future<void> load();
  List<ProductCategory> get allCategories;
  List<ProductCategory> get selectedCategories;
  void selectCategory(ProductCategory category);
  void unSelectCategory(ProductCategory category);
}

class ExploreRepositoryImp extends ExploreRepository {
  ExploreRepositoryImp({ProductDataProvider? productDataProvider})
      : _productDataProvider = productDataProvider ?? ProductDataProviderImp();
  final ProductDataProvider _productDataProvider;

  @override
  final List<ProductCategory> allCategories = [];
  @override
  final List<ProductCategory> selectedCategories = [];
  final List<Product> _products = [];

  @override
  Future<void> load() async {
    allCategories.clear();
    _products.clear();
    final categoryResult = await _productDataProvider.fetchCategories();
    if (categoryResult.isSuccess) {
      allCategories
          .addAll(List.from(categoryResult.data as List<ProductCategory>));
    } else {
      allCategories.clear();
    }

    final productResult = await _productDataProvider.fetchProducts();
    if (productResult.isSuccess) {
      _products.addAll(List.from(productResult.data as List<Product>));
    } else {
      _products.clear();
    }
  }

  @override
  ProcessResult<List<Product>> fetchFilteredProducts() {
    return ProcessResult.success(_products
        .where((product) {
          final categories = selectedCategories
              .where((category) => product.categories.contains(category))
              .toList();
          // return categories.isNotEmpty;
          return categories.length == selectedCategories.length;
        })
        .toSet()
        .toList());
  }

  @override
  void selectCategory(ProductCategory category) {
    if (!selectedCategories.contains(category)) {
      selectedCategories.add(category);
    }
  }

  @override
  void unSelectCategory(ProductCategory category) {
    if (selectedCategories.contains(category)) {
      selectedCategories.remove(category);
    }
  }
}
