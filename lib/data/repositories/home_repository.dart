/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 11:32:41

*/

import 'dart:collection';

import 'package:shoppy_rabbit/data/model/entities/banner.dart';
import 'package:shoppy_rabbit/data/model/entities/offer.dart';
import 'package:shoppy_rabbit/data/model/entities/product.dart';
import 'package:shoppy_rabbit/data/model/processing_models/process_result.dart';
import 'package:shoppy_rabbit/data/providers/product_data_provider.dart';
import 'package:shoppy_rabbit/data/providers/promotion_data_provider.dart';
import '../model/processing_models/process_result.dart';

abstract class UserHomeRepository {
  Future<ProcessResult<List<Product>>> fetchRecommendedProducts();
  Future<ProcessResult<List<Offer>>> fetchOffers();
  Future<ProcessResult<List<Banner>>> fetchPromotionBanners();
}

class UserHomeRepositoryImp extends UserHomeRepository {
  UserHomeRepositoryImp({
    ProductDataProvider? productDataProvider,
    PromotionDataProvider? promotionDataProvider,
  })  : _productDataProvider = productDataProvider ?? ProductDataProviderImp(),
        _promotionDataProvider =
            promotionDataProvider ?? PromotionDataProviderImp();
  final ProductDataProvider _productDataProvider;
  final PromotionDataProvider _promotionDataProvider;
  @override
  Future<ProcessResult<List<Offer>>> fetchOffers() {
    return _promotionDataProvider.fetchOffers();
  }

  @override
  Future<ProcessResult<List<Product>>> fetchRecommendedProducts() {
    return _productDataProvider.fetchProducts();
  }

  @override
  Future<ProcessResult<List<Banner>>> fetchPromotionBanners() {
    return _promotionDataProvider.fetchBanners();
  }
}
