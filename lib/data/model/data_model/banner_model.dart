/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 12:06:53

*/

import 'dart:convert';

class BannerModel {
  BannerModel({
    required this.name,
    required this.id,
    required this.image,
  });

  String name;
  String id;
  String image;

  static List<BannerModel> bannerModelFromJson(String str) =>
      List<BannerModel>.from(
          json.decode(str).map((x) => BannerModel.fromJson(x)));

  static String bannerModelToJson(List<BannerModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
  factory BannerModel.fromJson(Map<String, dynamic> json) => BannerModel(
        name: json["name"],
        id: json["id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
        "image": image,
      };
}
