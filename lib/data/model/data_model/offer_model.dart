/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 12:17:14

*/

import 'dart:convert';

List<OfferModel> offerModelFromJson(String str) =>
    List<OfferModel>.from(json.decode(str).map((x) => OfferModel.fromJson(x)));

String offerModelToJson(List<OfferModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OfferModel {
  OfferModel({
    required this.name,
    required this.id,
    required this.image,
    required this.type,
    required this.itemId,
  });

  String name;
  String id;
  String image;
  int type;
  String itemId;
  static List<OfferModel> offerModelFromJson(String str) =>
      List<OfferModel>.from(
          json.decode(str).map((x) => OfferModel.fromJson(x)));

  static String offerModelToJson(List<OfferModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
  factory OfferModel.fromJson(Map<String, dynamic> json) => OfferModel(
        name: json["name"],
        id: json["id"],
        image: json["image"],
        type: json["type"],
        itemId: json["item_id"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "id": id,
        "image": image,
        "type": type,
        "item_id": itemId,
      };
}
