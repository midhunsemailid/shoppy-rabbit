/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 18:34:11

*/
import 'dart:convert';

class CategoryModel {
  CategoryModel({
    required this.id,
    required this.name,
  });

  String id;
  String name;

  static List<CategoryModel> categoriesFromJson(String str) =>
      List<CategoryModel>.from(
          json.decode(str).map((x) => CategoryModel.fromJson(x)));
  static String productToJson(List<CategoryModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
