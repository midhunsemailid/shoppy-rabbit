/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 13:11:06

*/

import 'dart:convert';

import 'package:shoppy_rabbit/data/model/data_model/category_model.dart';

class ProductModel {
  ProductModel({
    required this.id,
    required this.name,
    required this.description,
    required this.images,
    required this.variants,
    required this.price,
    required this.categories,
  });
  static List<ProductModel> productsFromJson(String str) =>
      List<ProductModel>.from(
          json.decode(str).map((x) => ProductModel.fromJson(x)));
  static String productToJson(List<ProductModel> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
  String id;
  String name;
  String description;
  List<String> images;
  List<ProductVariantModel> variants;
  PriceModel price;
  List<CategoryModel> categories;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        images: List<String>.from(json["image"].map((x) => x)),
        variants: List<ProductVariantModel>.from(
            json["variants"].map((x) => ProductVariantModel.fromJson(x))),
        price: PriceModel.fromJson(json["price"]),
        categories: List<CategoryModel>.from(
            json["categories"].map((x) => CategoryModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "image": List<dynamic>.from(images.map((x) => x)),
        "variants": List<dynamic>.from(variants.map((x) => x.toJson())),
        "price": price.toJson(),
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
      };
}

class PriceModel {
  PriceModel({
    required this.amount,
    required this.offerAmount,
    required this.tax,
    required this.isOfferAvailable,
  });

  String amount;
  String offerAmount;
  TaxModel tax;
  bool isOfferAvailable;

  factory PriceModel.fromJson(Map<String, dynamic> json) => PriceModel(
        amount: json["amount"],
        isOfferAvailable: json["isOfferAvailable"],
        offerAmount: json["offer_amount"],
        tax: TaxModel.fromJson(json["tax"]),
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
        "offer_amount": offerAmount,
        "isOfferAvailable": isOfferAvailable,
        "tax": tax.toJson(),
      };
}

class TaxModel {
  TaxModel({
    required this.amount,
  });

  String amount;

  factory TaxModel.fromJson(Map<String, dynamic> json) => TaxModel(
        amount: json["amount"],
      );

  Map<String, dynamic> toJson() => {
        "amount": amount,
      };
}

class ProductVariantModel {
  ProductVariantModel({
    required this.id,
    required this.name,
    required this.attributes,
  });

  String id;
  String name;
  List<ProductAttributeModel> attributes;

  factory ProductVariantModel.fromJson(Map<String, dynamic> json) =>
      ProductVariantModel(
        id: json["id"],
        name: json["name"],
        attributes: List<ProductAttributeModel>.from(
            json["attribute"].map((x) => ProductAttributeModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "attribute": List<dynamic>.from(attributes.map((x) => x.toJson())),
      };
}

class ProductAttributeModel {
  ProductAttributeModel({
    required this.id,
    required this.value,
  });

  String id;
  String value;

  factory ProductAttributeModel.fromJson(Map<String, dynamic> json) =>
      ProductAttributeModel(
        id: json["id"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "value": value,
      };
}
