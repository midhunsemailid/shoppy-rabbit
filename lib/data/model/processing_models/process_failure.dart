/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 11:41:36

*/

import 'package:equatable/equatable.dart';

abstract class ProcessFailure extends Equatable implements Exception {
  @override
  String toString() => '$runtimeType Exception';

  @override
  List<Object> get props => [];
}

class DataLoadingFailure extends ProcessFailure {
  DataLoadingFailure(this.description);
  final String description;
}
