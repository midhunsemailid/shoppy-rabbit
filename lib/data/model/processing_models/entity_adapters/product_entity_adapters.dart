/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 14:28:35

*/

import 'package:shoppy_rabbit/data/model/data_model/category_model.dart';
import 'package:shoppy_rabbit/data/model/data_model/product_model.dart';
import 'package:shoppy_rabbit/data/model/entities/price.dart';
import 'package:shoppy_rabbit/data/model/entities/product.dart';

/// Converting Data Models to Entities.
extension ProductEntityAdapter on Product {
  static Product fromDataModel(ProductModel dataModel) {
    return Product(
      id: dataModel.id,
      description: dataModel.description,
      name: dataModel.name,
      images: dataModel.images,
      categories: dataModel.categories
          .map((category) =>
              ProductCategoryEntityAdapter.fromDataModel(category))
          .toList(),
      price: ProductPriceEntityAdapter.fromDataModel(dataModel.price),
      variants: dataModel.variants
          .map((variant) => ProductVariantEntityAdapter.fromDataModel(variant))
          .toList(),
    );
  }
}

extension ProductVariantEntityAdapter on ProductVariant {
  static ProductVariant fromDataModel(ProductVariantModel dataModel) {
    return ProductVariant(
      attributes: dataModel.attributes
          .map((attribute) =>
              ProductAttributeEntityAdapter.fromDataModel(attribute))
          .toList(),
      id: dataModel.id,
      name: dataModel.name,
    );
  }
}

extension ProductPriceEntityAdapter on Price {
  static Price fromDataModel(PriceModel dataModel) {
    return Price(
      amount: dataModel.amount,
      offerAmount: dataModel.offerAmount,
      isOfferAvailable: dataModel.isOfferAvailable,
    );
  }
}

extension ProductAttributeEntityAdapter on ProductAttribute {
  static ProductAttribute fromDataModel(ProductAttributeModel dataModel) {
    return ProductAttribute(
      id: dataModel.id,
      value: dataModel.value,
    );
  }
}

extension ProductCategoryEntityAdapter on ProductCategory {
  static ProductCategory fromDataModel(CategoryModel dataModel) {
    return ProductCategory(id: dataModel.id, name: dataModel.name);
  }
}
