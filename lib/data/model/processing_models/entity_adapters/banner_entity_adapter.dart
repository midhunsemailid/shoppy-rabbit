/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 17:03:22

*/
import 'package:shoppy_rabbit/data/model/data_model/banner_model.dart';
import 'package:shoppy_rabbit/data/model/entities/banner.dart';

extension BannerEntityAdapter on Banner {
  static Banner fromDataModel(BannerModel dataModel) {
    return Banner(
      id: dataModel.id,
      image: dataModel.image,
      name: dataModel.name,
    );
  }
}
