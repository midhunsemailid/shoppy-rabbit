/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 17:00:20

*/

import 'package:shoppy_rabbit/data/model/data_model/offer_model.dart';
import 'package:shoppy_rabbit/data/model/entities/offer.dart';

extension OfferEntityAdapter on Offer {
  static Offer fromDataModel(OfferModel dataModel) {
    return Offer(
      id: dataModel.id,
      image: dataModel.image,
      itemId: dataModel.itemId,
      name: dataModel.name,
      type: dataModel.type,
    );
  }
}
