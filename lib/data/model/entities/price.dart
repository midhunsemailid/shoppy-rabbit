/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 11:15:12

*/

class Price {
  Price({
    required this.amount,
    required this.isOfferAvailable,
    required this.offerAmount,
  });
  final String amount;
  final String offerAmount;
  final bool isOfferAvailable;
}

class Tax {
  Tax({
    required this.amount,
  });
  String amount;
}
