/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 11:15:29

*/

class Banner {
  Banner({
    required this.name,
    required this.id,
    required this.image,
  });

  final String name;
  final String id;
  final String image;
}
