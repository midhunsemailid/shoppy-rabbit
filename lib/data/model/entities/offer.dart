/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 11:15:21

*/

class Offer {
  Offer({
    required this.name,
    required this.id,
    required this.image,
    required this.type,
    required this.itemId,
  });

  final String name;
  final String id;
  final String image;
  final int type;
  final String itemId;
}
