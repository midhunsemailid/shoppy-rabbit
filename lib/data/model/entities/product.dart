/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 10:58:55

*/

import 'package:equatable/equatable.dart';
import 'package:shoppy_rabbit/data/model/entities/price.dart';

class ProductAttribute {
  ProductAttribute({
    required this.id,
    required this.value,
  });
  String id;
  String value;
}

class ProductVariant {
  ProductVariant({
    required this.id,
    required this.name,
    required this.attributes,
  });
  String id;
  String name;
  List<ProductAttribute> attributes;
}

class Product extends Equatable {
  const Product({
    required this.description,
    required this.id,
    required this.name,
    required this.variants,
    required this.price,
    required this.images,
    required this.categories,
  });
  final String id;
  final String name;
  final String description;
  final List<ProductVariant> variants;
  final List<String> images;
  final Price price;
  final List<ProductCategory> categories;

  @override
  List<Object?> get props => [
        id,
      ];
}

class ProductCategory extends Equatable {
  const ProductCategory({
    required this.id,
    required this.name,
  });
  final String id;
  final String name;

  @override
  List<Object?> get props => [id, name];
}
