/*

 Author: Midhun P Mathew
 Email: midhun.mathew@ibsplc.com

 Creation Date: 2022-04-14 10:55:56

*/

import 'package:flutter/material.dart';
import 'package:shoppy_rabbit/presentation/bottom_navigation/bottom_navigation.dart';
import 'package:shoppy_rabbit/utils/primary_swatch_color.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: PrimarySwatchColor(Colors.white).value,
      ),
      home: const AppBottomNavigationPage(),
    );
  }
}
