import 'package:flutter/material.dart';
import 'package:shoppy_rabbit/app/app.dart';

void main() {
  runApp(const App());
}
